<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
  <link rel="stylesheet" href="theme.css" type="text/css">
</head>
<title>Register</title>
<body>
  <?php
    session_start();
    $img = $_SESSION["image"];
    

  ?>
  <div class="container">
        
        <div class="user_input">
          <lable>Họ và tên</lable>
          <div class="user_info"><?php echo $_SESSION["Regist"]["fullName"]; ?></div>
        </div>

        <div class="user_input">
          <lable>Giới tính
          </lable>
          <div class="user_info"><?php echo $_SESSION["Regist"]["gender"]; ?></div>
        </div>

        <div class="user_input">
          <lable>Phân khoa</lable>
          <div class="user_info"><?php echo $_SESSION["Regist"]["khoa"]; ?></div>
        </div>

        <div class="user_input" date-date-format="dd/MM/yyyy">
          <lable>Ngày sinh</lable>
          <div class="user_info"><?php echo $_SESSION["Regist"]["birthday"]; ?></div>
        </div>

        <div class="user_input">
          <lable>Địa chỉ</lable>
          <div class="user_info"><?php echo $_SESSION["Regist"]["address"]; ?></div>
        </div>

        <div class="user_input">
          <lable>Hình ảnh</lable>
          <div class="user_info">
            <img src="upload/<?= $img?>" alt="user image" width="160px" height="100px" class="image">


        <div class ="user_submit">
          <input type="submit" name="btnSubmit" id="form_btn" value="Xác nhận">
        </div>

  </div>
  <?php
  ?>
  <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
  <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
  <!-- Bootstrap -->
  <!-- Bootstrap DatePicker -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
  <!-- Bootstrap DatePicker -->
  <script type="text/javascript">
    $(function() {
      $('#birthday').datepicker({
        format: "dd/mm/yyyy"
      });
    });
  </script>
  
</body>

</html>